# CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Credits

## INTRODUCTION

This module provides videotool handler for video embed field module now with
this module you can add embed videos from https://media.videotool.dk/ to
your website.

Based on https://www.drupal.org/project/video_embed_ted

## REQUIREMENTS

- Video Embed Field
  https://drupal.org/project/video_embed_field

## INSTALLATION

1. Install and enable the module as usual.
2. In video embed field settings enable Videotool provider.

## CREDITS

### Authors of video_embed_videotool

* Andreas Albers (Eksponent Aps)

### Authors of video_embed_ted

* Audrius Vaitonis (deepapple)
* BrightLemon - We create communities that increase engagement
  and participation - http://brightlemon.com
