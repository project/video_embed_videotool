<?php

/**
 * @file
 * Contains
 *   \Drupal\video_embed_videotool\Plugin\video_embed_field\Provider\Videotool.
 */

namespace Drupal\video_embed_videotool\Plugin\video_embed_field\Provider;

use DOMDocument;
use DOMXPath;
use Drupal\video_embed_field\ProviderPluginBase;
use Guzzle\Http\Client;
use Drupal\Component\Utility\SafeMarkup;

/**
 * @VideoEmbedProvider(
 *   id = "videotool",
 *   title = @Translation("Videotool")
 * )
 */
class Videotool extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => 'videotool',
      '#url' => $this->getVideotoolUrl(),
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'allow' => 'autoplay; fullscreen',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $image_url = $this->getOGImage();
    if ($image_url) {
      return $image_url;
    }
    return FALSE;
  }

  /**
   * Gets thumbnail from og:image metatag.
   *
   * @return mixed
   */
  public function getOGImage() {
    $doc = new DomDocument();
    $video_url = $this->getVideotoolUrl();
    $doc->loadHTML(file_get_contents($video_url));
    $xpath = new DOMXPath($doc);
    $image_node = $xpath->query('//*/meta[starts-with(@property, \'og:image\')]');
    if (isset($image_node[0])) {
      $image_url = $image_node[0]->getAttribute('content');
      return $image_url;
    }
    return FALSE;
  }

  /**
   * Build video url with channel id and video id.
   *
   * @return string
   */
  public function getVideotoolUrl() {
    [$channel_id, $video_id] = $this->getVideoId();
    return 'https://media.videotool.dk/?vn=' . $channel_id . '_' . $video_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalThumbnailUri() {
    [$channel_id, $video_id] = $this->getVideoId();
    return $this->thumbsDirectory . '/' . $channel_id . '_' . $video_id . '.png';
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $matches = [];
    preg_match("(https:\/\/media\.videotool\.dk\/\?vn=([a-z0-9]{3})_([a-z0-9]{28}))", $input, $matches);
    if ($matches && !empty($matches[1])) {
      return [$matches[1], $matches[2]];
    }
    return FALSE;
  }

}
